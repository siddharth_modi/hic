//
//  HomeScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "HomeScreen.h"

@interface HomeScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientGetList,*ClientNoti;
    
    HTTPClient *bgFetchHTTPClient;
    NSMutableDictionary *DicServicesData;
    NSMutableArray *ArrTodayArt;
    NSMutableArray *ArrListAlarm;
    BOOL isTokonValid;
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
}
@property (nonatomic, strong) HTTPClient *httpClient;

@end


@implementation HomeScreen
@synthesize tblListAlarm;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
   
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    FirstTabItem.tabState = TabStateEnabled;
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    // ThirdTabItem.tabState = TabStateEnabled;
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];

    
    [self GetApiData];
    
    self.tblListAlarm.tableFooterView = [[UIView alloc] init];
    self.tblListAlarm.backgroundColor=[UIColor clearColor];
}
#pragma mark - SetNotiFication
-(void)SetNotiFication
{
    [SVProgressHUD show];
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    //NSLog(@"output is : %@", Identifier);
    
    NSString *divid=[[NSUserDefaults standardUserDefaults]valueForKey:@"DivID"];
    
    if (divid ==nil)
    {
        divid=@"1234-testing-deviciceID-40593960123";
    }
    NSDictionary *parameters = @{
                                 @"regID_ios" :divid,
                                 @"devicePlatform" :KDevicePlatform,
                                 @"deviceUUID": Identifier
                                 };
    
    NSString *registerURL = [NSString stringWithFormat:@"%@pushnotification_web.php",kAPIURL];
    ClientNoti = [[HTTPClient alloc] init];
    ClientNoti.delegate = self;
    [ClientNoti getResponseFromAPI:registerURL andParameters:parameters];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
-(void)viewWillAppear:(BOOL)animated
{
   // [self SetNotiFication];
}

#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==1)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark - GetApiData
-(void)GetApiData
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@get_salat_data_web.php",kAPIURL];
    ClientGetList = [[HTTPClient alloc] init];
    ClientGetList.delegate = self;
    [ClientGetList getResponseFromAPI:registerURL andParameters:nil];
}
#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientGetList)
    {
        DicServicesData=response;
        ArrTodayArt=[[NSMutableArray alloc]init];
        ArrTodayArt=[[response objectForKey:@"salat"] objectForKey:@"today"];
        [tblListAlarm reloadData];
        
    }
    else if (client== ClientNoti)
    {
        NSString *code=[response objectForKey:@"code"];
        if ([code isEqualToString:@"200"])
        {
            
        }
    }
    
    /*
    else if (client == bgFetchHTTPClient)
    {
        DicServicesData=response;
        ArrTodayArt=[[NSMutableArray alloc]init];
        ArrTodayArt=[[response objectForKey:@"salat"] objectForKey:@"today"];
        
        NSString *strdate=[ArrTodayArt valueForKey:@"prayer_date"];
        NSString *alarm1,*alarm2,*alarm3,*alarm4,*alarm5,*alarm6,*alarm7;
        
        
        NSString *Tmimsaak=[ArrTodayArt valueForKey:@"imsaak"];
        NSString *TmFajr=[ArrTodayArt valueForKey:@"fajr"];
        NSString *Tmsunrise=[ArrTodayArt valueForKey:@"sunrise"];
        NSString *Tmzohar=[ArrTodayArt valueForKey:@"zohar"];
        NSString *Tmsunset=[ArrTodayArt valueForKey:@"sunset"];
        NSString *Tmmaghrib=[ArrTodayArt valueForKey:@"maghrib"];
        NSString *Tmmidnight=[ArrTodayArt valueForKey:@"midnight"];
        
        alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmimsaak];
        alarm2=[NSString stringWithFormat:@"%@ %@",strdate,TmFajr];
        alarm3=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunrise];
        alarm4=[NSString stringWithFormat:@"%@ %@",strdate,Tmzohar];
        alarm5=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunset];
        alarm6=[NSString stringWithFormat:@"%@ %@",strdate,Tmmaghrib];
        alarm7=[NSString stringWithFormat:@"%@ %@",strdate,Tmmidnight];
        
        NSLog(@"ala 1 Time :- %@",alarm1);
        //    NSLog(@"ala 2 Time :- %@",alarm2);
        //    NSLog(@"ala 3 Time :- %@",alarm3);
        //    NSLog(@"ala 4 Time :- %@",alarm4);
        //    NSLog(@"ala 5 Time :- %@",alarm5);
        //    NSLog(@"ala 6 Time :- %@",alarm6);
        //    NSLog(@"ala 7 Time :- %@",alarm7);

        //Check which switches are on ?
        NSString *str1=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch1"];
        if (str1 !=nil)
        {
            if ([str1 isEqualToString:@"on"])
            {
               // [cell.Switch1 setSelectedSegmentIndex:1];
                //cell.img1.image=[UIImage imageNamed:@"green_alamIcon"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm1];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Imsaak Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Imsaak";
                    //localNotification.alertAction =@"Imsaak";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                    [userinfo setValue:@"1" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

                
            }
            else
            {
               // [cell.Switch1 setSelectedSegmentIndex:0];
               // cell.img1.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch1 setSelectedSegmentIndex:0];
            //cell.img1.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str2=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch2"];
        if (str2 !=nil)
        {
            if ([str2 isEqualToString:@"on"])
            {
               // [cell.Switch2 setSelectedSegmentIndex:1];
               // cell.img2.image=[UIImage imageNamed:@"green_alamIcon"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm2];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Fajr Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Fajr";
                    //localNotification.alertAction =@"Fajr";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                    [userinfo setValue:@"2" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
                //[cell.Switch2 setSelectedSegmentIndex:0];
               // cell.img2.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch2 setSelectedSegmentIndex:0];
            //cell.img2.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str3=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch3"];
        if (str3 !=nil)
        {
            if ([str3 isEqualToString:@"on"])
            {
               // [cell.Switch3 setSelectedSegmentIndex:1];
               // cell.img3.image=[UIImage imageNamed:@"green_alamIcon"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm3];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Sunrise Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Sunrise";
                    //localNotification.alertAction =@"Sunrise";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                    [userinfo setValue:@"3" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
               // [cell.Switch3 setSelectedSegmentIndex:0];
               // cell.img3.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch3 setSelectedSegmentIndex:0];
           // cell.img3.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str4=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch4"];
        if (str4 !=nil)
        {
            if ([str4 isEqualToString:@"on"])
            {
              //  [cell.Switch4 setSelectedSegmentIndex:1];
              //  cell.img4.image=[UIImage imageNamed:@"green_alamIcon"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm4];
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Zohar Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Zohar";
                    //localNotification.alertAction =@"Zohar";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                    [userinfo setValue:@"4" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
              //  [cell.Switch4 setSelectedSegmentIndex:0];
              //  cell.img4.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch4 setSelectedSegmentIndex:0];
            //cell.img4.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str5=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch5"];
        if (str5 !=nil)
        {
            if ([str5 isEqualToString:@"on"])
            {
            //    [cell.Switch5 setSelectedSegmentIndex:1];
            //    cell.img5.image=[UIImage imageNamed:@"green_alamIcon"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm5];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Sunset Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    //localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Sunset";
                    //localNotification.alertAction =@"Sunset";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                    [userinfo setValue:@"5" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
             //   [cell.Switch5 setSelectedSegmentIndex:0];
            //    cell.img5.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch5 setSelectedSegmentIndex:0];
           // cell.img5.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str6=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch6"];
        if (str6 !=nil)
        {
            if ([str6 isEqualToString:@"on"])
            {
            //    [cell.Switch6 setSelectedSegmentIndex:1];
           //     cell.img6.image=[UIImage imageNamed:@"green_alamIcon"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm6];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Maghrib Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                    localNotification.alertBody = @"Maghrib";
                    //localNotification.alertAction =@"Maghrib";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                    [userinfo setValue:@"6" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
           //     [cell.Switch6 setSelectedSegmentIndex:0];
           //     cell.img6.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch6 setSelectedSegmentIndex:0];
           // cell.img6.image=[UIImage imageNamed:@"red_alamIcon"];
        }
        NSString *str7=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch7"];
        if (str7 !=nil)
        {
            if ([str7 isEqualToString:@"on"])
            {
            //    [cell.Switch7 setSelectedSegmentIndex:1];
           //     cell.img7.image=[UIImage imageNamed:@"green_alamIcon"];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate * dateis = [dateFormatter dateFromString: alarm7];
                
                [self dateComparision:dateis];
                
                if (isTokonValid ==YES)
                {
                    NSLog(@"Midnight Alarm on");
                    
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = dateis;
                    
                    // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:59];
                    localNotification.alertBody = @"Midnight";
                    //localNotification.alertAction =@"Midnight";
                    localNotification.soundName = @"alarm_sound.m4r";
                    localNotification.timeZone = [NSTimeZone defaultTimeZone];
                    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                    NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                    [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                    [userinfo setValue:@"7" forKey:@"NT"];
                    localNotification.userInfo=userinfo;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                }

            }
            else
            {
            //    [cell.Switch7 setSelectedSegmentIndex:0];
           //     cell.img7.image=[UIImage imageNamed:@"red_alamIcon"];
            }
        }
        else
        {
           // [cell.Switch7 setSelectedSegmentIndex:0];
           // cell.img7.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }*/
    
}

- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(NSDate*)StringToNotidate:(NSString *)Str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * dateis = [dateFormatter dateFromString: Str];
    return dateis;
    
}
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (ArrTodayArt!=nil)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    HomeCell *cell = (HomeCell *)[tv dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;

    cell.lbldayname.text=[self GetDayName:[ArrTodayArt valueForKey:@"prayer_date"]];
    cell.lblyearname1.text=[self changeYerFormate:[ArrTodayArt valueForKey:@"prayer_date"]];
    
    NSString *d1=[ArrTodayArt valueForKey:@"hijri_day"];
    NSString *d2=[ArrTodayArt valueForKey:@"hijri_mnth_name"];
    NSString *d3=[ArrTodayArt valueForKey:@"hijri_year"];
    NSString *yersecond=[NSString stringWithFormat:@"%@ %@ %@",d1,d2,d3];
    
    cell.lblYearName2.text=yersecond;
    
    cell.lblImsaakTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"imsaak"]];
    cell.lblFajrTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"fajr"]];
    cell.lblSunriseTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"sunrise"] ];
    cell.lblZoharTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"zohar"] ];
    cell.lblSunsetTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"sunset"] ];
    cell.lblMaghribTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"maghrib"] ];
    cell.lblMidnightTime.text=[self TimeFormateChange:[ArrTodayArt valueForKey:@"midnight"] ];
    
    //Switch1
    [cell.Switch1 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch1 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch1.tag=1;
    [cell.Switch1 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str1=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch1"];
    if (str1 !=nil)
    {
        if ([str1 isEqualToString:@"on"])
        {
            
            [cell.Switch1 setSelectedSegmentIndex:1];
            cell.img1.image=[UIImage imageNamed:@"green_alamIcon"];
            
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"imsaak"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmimsaak];

            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"imsaak"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmimsaak2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"imsaak"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmimsaak3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"imsaak"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmimsaak4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"imsaak"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmimsaak5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            
            NSLog(@"%@ ",[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"]);
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"]objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmimsaak6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"imsaak"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmimsaak6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Imsaak";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
                [userinfo setValue:@"1" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            [cell.Switch1 setSelectedSegmentIndex:0];
            cell.img1.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch1 setSelectedSegmentIndex:0];
        cell.img1.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    
    //Switch2
    [cell.Switch2 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch2 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch2.tag=2;
    [cell.Switch2 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str2=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch2"];
    if (str2 !=nil)
    {
        if ([str2 isEqualToString:@"on"])
        {
            [cell.Switch2 setSelectedSegmentIndex:1];
            cell.img2.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"fajr"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,TmFajr];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"fajr"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,TmFajr2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"fajr"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,TmFajr3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"fajr"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,TmFajr4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"fajr"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,TmFajr5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *TmFajr6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"fajr"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,TmFajr6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            [cell.Switch2 setSelectedSegmentIndex:0];
            cell.img2.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch2 setSelectedSegmentIndex:0];
        cell.img2.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    
    //Switch3
    [cell.Switch3 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch3 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch3.tag=3;
    [cell.Switch3 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str3=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch3"];
    if (str3 !=nil)
    {
        if ([str3 isEqualToString:@"on"])
        {
            [cell.Switch3 setSelectedSegmentIndex:1];
            cell.img3.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"sunrise"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunrise];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"sunrise"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmsunrise2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"sunrise"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmsunrise3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"sunrise"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmsunrise4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"sunrise"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmsunrise5];
            
            NSDate *dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunrise6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"sunrise"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmsunrise6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication]applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            [cell.Switch3 setSelectedSegmentIndex:0];
            cell.img3.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch3 setSelectedSegmentIndex:0];
        cell.img3.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    

    
    //Switch4
    [cell.Switch4 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch4 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch4.tag=4;
    [cell.Switch4 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    NSString *str4=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch4"];
    if (str4 !=nil)
    {
        if ([str4 isEqualToString:@"on"])
        {
            [cell.Switch4 setSelectedSegmentIndex:1];
            cell.img4.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"zohar"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmzohar];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"zohar"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmzohar2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"zohar"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmzohar3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"zohar"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmzohar4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"zohar"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmzohar5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmzohar6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"zohar"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmzohar6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
        }
        else
        {
            [cell.Switch4 setSelectedSegmentIndex:0];
            cell.img4.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch4 setSelectedSegmentIndex:0];
        cell.img4.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    
    //Switch5
    [cell.Switch5 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch5 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch5.tag=5;
    [cell.Switch5 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str5=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch5"];
    if (str5 !=nil)
    {
        if ([str5 isEqualToString:@"on"])
        {
            [cell.Switch5 setSelectedSegmentIndex:1];
            cell.img5.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"sunset"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunset];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"sunset"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmsunset2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"sunset"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmsunset3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"sunset"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmsunset4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"sunset"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmsunset5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmsunset6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"sunset"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmsunset6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            [cell.Switch5 setSelectedSegmentIndex:0];
            cell.img5.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch5 setSelectedSegmentIndex:0];
        cell.img5.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    //Switch6
    [cell.Switch6 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch6 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch6.tag=6;
    [cell.Switch6 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str6=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch6"];
    if (str6 !=nil)
    {
        if ([str6 isEqualToString:@"on"])
        {
            [cell.Switch6 setSelectedSegmentIndex:1];
            cell.img6.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"maghrib"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmmaghrib];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"maghrib"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmmaghrib2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"maghrib"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmmaghrib3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"maghrib"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmmaghrib4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"maghrib"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmmaghrib5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmaghrib6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"maghrib"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmmaghrib6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
        }
        else
        {
            [cell.Switch6 setSelectedSegmentIndex:0];
            cell.img6.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch6 setSelectedSegmentIndex:0];
        cell.img6.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    
    
    
    //Switch7
    [cell.Switch7 setDividerImage:[UIImage imageNamed:@"off"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [cell.Switch7 setDividerImage:[UIImage imageNamed:@"on"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    cell.Switch7.tag=7;
    [cell.Switch7 addTarget:self action:@selector(segmentSwitch:)forControlEvents:UIControlEventValueChanged];
    
    NSString *str7=[[NSUserDefaults standardUserDefaults]valueForKey:@"Switch7"];
    if (str7 !=nil)
    {
        if ([str7 isEqualToString:@"on"])
        {
            [cell.Switch7 setSelectedSegmentIndex:1];
            cell.img7.image=[UIImage imageNamed:@"green_alamIcon"];
            
            //tomtomorrow
            NSString *strdate=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"tomorrow"] objectForKey:@"midnight"];
            
            NSString *alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmmidnight];
            
            NSDate * dateis=[self StringToNotidate:alarm1];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_tomorrow
            NSString *strdate2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight2=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_tomorrow"] objectForKey:@"midnight"];
            
            NSString *alarm2=[NSString stringWithFormat:@"%@ %@",strdate2,Tmmidnight2];
            
            NSDate * dateis2=[self StringToNotidate:alarm2];
            
            [self dateComparision:dateis2];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis2;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_4_prayer_time
            NSString *strdate3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight3=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_4_prayer_time"] objectForKey:@"midnight"];
            
            NSString *alarm3=[NSString stringWithFormat:@"%@ %@",strdate3,Tmmidnight3];
            
            NSDate * dateis3=[self StringToNotidate:alarm3];
            
            [self dateComparision:dateis3];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis3;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_5_prayer_time
            NSString *strdate4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight4=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_5_prayer_time"] objectForKey:@"midnight"];
            
            NSString *alarm4=[NSString stringWithFormat:@"%@ %@",strdate4,Tmmidnight4];
            
            NSDate * dateis4=[self StringToNotidate:alarm4];
            
            [self dateComparision:dateis4];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis4;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_6_prayer_time
            NSString *strdate5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight5=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_6_prayer_time"] objectForKey:@"midnight"];
            
            NSString *alarm5=[NSString stringWithFormat:@"%@ %@",strdate5,Tmmidnight5];
            
            NSDate * dateis5=[self StringToNotidate:alarm5];
            
            [self dateComparision:dateis5];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis5;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            //day_after_7_prayer_time
            NSString *strdate6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"prayer_date"];
            
            NSString *Tmmidnight6=[[[DicServicesData objectForKey:@"salat"] objectForKey:@"day_after_7_prayer_time"] objectForKey:@"midnight"];
            
            NSString *alarm6=[NSString stringWithFormat:@"%@ %@",strdate6,Tmmidnight6];
            
            NSDate * dateis6=[self StringToNotidate:alarm6];
            
            [self dateComparision:dateis6];
            
            if (isTokonValid ==YES)
            {
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis6;
                localNotification.alertBody = @"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            [cell.Switch7 setSelectedSegmentIndex:0];
            cell.img7.image=[UIImage imageNamed:@"red_alamIcon"];
        }
    }
    else
    {
        [cell.Switch7 setSelectedSegmentIndex:0];
        cell.img7.image=[UIImage imageNamed:@"red_alamIcon"];
    }
    
    return cell;
}


- (NSIndexPath *)indexPathWithSubview:(UIView *)subview
{
    while (![subview isKindOfClass:[UITableViewCell self]] && subview)
    {
        subview = subview.superview;
    }
    return [self.tblListAlarm indexPathForCell:(UITableViewCell *)subview];
}

#pragma mark - Alarm Swich On/Off Clicked
- (IBAction)segmentSwitch:(UISegmentedControl *)sender
{
    
    NSIndexPath *path = [self indexPathWithSubview:(UIButton *)sender];
    HomeCell* cell = (HomeCell *)[self.tblListAlarm cellForRowAtIndexPath:path];
    
    
    
    NSString *strdate=[ArrTodayArt valueForKey:@"prayer_date"];
    NSString *alarm1,*alarm2,*alarm3,*alarm4,*alarm5,*alarm6,*alarm7;
    
    
    NSString *Tmimsaak=[ArrTodayArt valueForKey:@"imsaak"];
    NSString *TmFajr=[ArrTodayArt valueForKey:@"fajr"];
    NSString *Tmsunrise=[ArrTodayArt valueForKey:@"sunrise"];
    NSString *Tmzohar=[ArrTodayArt valueForKey:@"zohar"];
    NSString *Tmsunset=[ArrTodayArt valueForKey:@"sunset"];
    NSString *Tmmaghrib=[ArrTodayArt valueForKey:@"maghrib"];
    NSString *Tmmidnight=[ArrTodayArt valueForKey:@"midnight"];

    alarm1=[NSString stringWithFormat:@"%@ %@",strdate,Tmimsaak];
    alarm2=[NSString stringWithFormat:@"%@ %@",strdate,TmFajr];
    alarm3=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunrise];
    alarm4=[NSString stringWithFormat:@"%@ %@",strdate,Tmzohar];
    alarm5=[NSString stringWithFormat:@"%@ %@",strdate,Tmsunset];
    alarm6=[NSString stringWithFormat:@"%@ %@",strdate,Tmmaghrib];
    alarm7=[NSString stringWithFormat:@"%@ %@",strdate,Tmmidnight];

    
   // NSArray *allNoti = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *allNoti = [app scheduledLocalNotifications];
    
    NSInteger selectedSegment = sender.selectedSegmentIndex;
    if (selectedSegment == 0)
    {
        if (sender.tag==1)
        {
            NSLog(@"switch1 off");
            cell.img1.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch1"];
            
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"1"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
        }
        else if (sender.tag==2)
        {
            NSLog(@"switch2 off");
            cell.img2.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch2"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"2"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
        }
        else if (sender.tag==3)
        {
            NSLog(@"switch3 off");
            cell.img3.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch3"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"3"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
            
        }
        else if (sender.tag==4)
        {
             NSLog(@"switch4 off");
            cell.img4.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch4"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"4"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
        }
        else if (sender.tag==5)
        {
            NSLog(@"switch5 off");
            cell.img5.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch5"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"5"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
            
        }
        else if (sender.tag==6)
        {
            NSLog(@"switch6 off");
            cell.img6.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch6"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"6"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
            
        }
        else
        {
            NSLog(@"switch7 off");
            cell.img7.image=[UIImage imageNamed:@"red_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"off" forKey:@"Switch7"];
            
            //remove notification code
            //NSLog(@"%@",allNoti);
            for (int i=0; i<[allNoti count]; i++)
            {
                UILocalNotification* oneEvent = [allNoti objectAtIndex:i];
                NSDictionary *userInfoCurrent = oneEvent.userInfo;
                NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"NT"]];
                if ([uid isEqualToString:@"7"])
                {
                    [app cancelLocalNotification:oneEvent];
                    break;
                }
            }
            //
            
        }
        
    }
    else
    {
        if (sender.tag==1)
        {
            cell.img1.image=[UIImage imageNamed:@"green_alamIcon"];
            NSLog(@"switch1 on");
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch1"];
            
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate * dateis = [dateFormatter dateFromString: alarm1];
            
            [self dateComparision:dateis];
            
           if (isTokonValid ==YES)
           {
               NSLog(@"Imsaak Alarm on");
               
               UILocalNotification* localNotification = [[UILocalNotification alloc] init];
               localNotification.fireDate = dateis;
               
               // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
               localNotification.alertBody = @"Imsaak";
               //localNotification.alertAction =@"Imsaak";
               localNotification.soundName = @"alarm_sound.m4r";
               localNotification.timeZone = [NSTimeZone defaultTimeZone];
               localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
               NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
               [userinfo setValue:@"Imsaak" forKey:@"AlarmName"];
               [userinfo setValue:@"1" forKey:@"NT"];
               localNotification.userInfo=userinfo;
               
               [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
           }
        }
        else if (sender.tag==2)
        {
            NSLog(@"switch2 on");
            cell.img2.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch2"];
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm2];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                NSLog(@"Fajr Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                localNotification.alertBody = @"Fajr";
                //localNotification.alertAction =@"Fajr";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Fajr" forKey:@"AlarmName"];
                [userinfo setValue:@"2" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else if (sender.tag==3)
        {
            NSLog(@"switch3 on");
            cell.img3.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch3"];
            
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm3];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                 NSLog(@"Sunrise Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                localNotification.alertBody = @"Sunrise";
                //localNotification.alertAction =@"Sunrise";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunrise" forKey:@"AlarmName"];
                [userinfo setValue:@"3" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            
            
        }
        else if (sender.tag==4)
        {
            NSLog(@"switch4 on");
            cell.img4.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch4"];
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm4];
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                NSLog(@"Zohar Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                localNotification.alertBody = @"Zohar";
                //localNotification.alertAction =@"Zohar";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Zohar" forKey:@"AlarmName"];
                [userinfo setValue:@"4" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else if (sender.tag==5)
        {
            NSLog(@" Sunset switch5 on");
            cell.img5.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch5"];
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm5];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                NSLog(@"Sunset Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                //localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                localNotification.alertBody = @"Sunset";
                //localNotification.alertAction =@"Sunset";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Sunset" forKey:@"AlarmName"];
                [userinfo setValue:@"5" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else if (sender.tag==6)
        {
            NSLog(@"Maghrib switch6 on");
            cell.img6.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch6"];
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm6];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                NSLog(@"Maghrib Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
                localNotification.alertBody = @"Maghrib";
                //localNotification.alertAction =@"Maghrib";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Maghrib" forKey:@"AlarmName"];
                [userinfo setValue:@"6" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
        else
        {
            NSLog(@"switch7 on");
            cell.img7.image=[UIImage imageNamed:@"green_alamIcon"];
            [[NSUserDefaults standardUserDefaults]setObject:@"on" forKey:@"Switch7"];
            
            
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        NSDate * dateis = [dateFormatter dateFromString: alarm7];
            
            [self dateComparision:dateis];
            
            if (isTokonValid ==YES)
            {
                NSLog(@"Midnight Alarm on");
                
                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = dateis;
                
                // localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:59];
                localNotification.alertBody = @"Midnight";
                //localNotification.alertAction =@"Midnight";
                localNotification.soundName = @"alarm_sound.m4r";
                localNotification.timeZone = [NSTimeZone defaultTimeZone];
                localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
                [userinfo setValue:@"Midnight" forKey:@"AlarmName"];
                [userinfo setValue:@"7" forKey:@"NT"];
                localNotification.userInfo=userinfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
        }
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [tblListAlarm reloadData];
}

#pragma mark - Date changer Method
-(NSString *)changeYerFormate:(NSString *)dateis
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *date = [dateFormatter dateFromString:dateis];
    dateFormatter.dateFormat = @"dd MMMM yyyy";
    return [dateFormatter stringFromDate:date];
}
-(NSString *)GetDayName:(NSString *)dateis
{
    NSString *dateString =[ArrTodayArt valueForKey:@"prayer_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [[NSDate alloc] init];
    date = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:date];
}
-(NSString *)TimeFormateChange:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:date];
}

#pragma mark- Curent date and time
- (NSDate *)currentDateandTime
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateString = today;
    return dateString;
}

-(BOOL)dateComparision:(NSDate*)date1
{
    
    NSDate *CurrentDate=[self currentDateandTime];
    
    if ([date1 compare:CurrentDate] == NSOrderedDescending)
    {
        NSLog(@"curent date is less");
        isTokonValid = YES;
    }
    else if ([date1 compare:CurrentDate] == NSOrderedAscending)
    {
        NSLog(@"server date is less");
        isTokonValid = NO;
    }
    else
    {
        NSLog(@"Both dates are same");
         isTokonValid = YES;
    }
    return isTokonValid;
    
    
    
}

#pragma mark -
#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


////bg Fatch


- (void)alarmSchedulerWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSString *registerURL = [NSString stringWithFormat:@"%@get_salat_data_web.php",kAPIURL];
    bgFetchHTTPClient = [[HTTPClient alloc] init];
    bgFetchHTTPClient.delegate = self;
    [bgFetchHTTPClient getResponseFromAPI:registerURL andParameters:nil];
    
    
    
    /*
     At the end of the fetch, invoke the completion handler.
     */
    completionHandler(UIBackgroundFetchResultNewData);
}







@end
