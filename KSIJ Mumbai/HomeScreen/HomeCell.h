//
//  HomeCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 24/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCell : UITableViewCell
{
    
}
@property (strong, nonatomic) IBOutlet UILabel *lbldayname;
@property (strong, nonatomic) IBOutlet UILabel *lblyearname1;
@property (strong, nonatomic) IBOutlet UILabel *lblYearName2;


@property (strong, nonatomic) IBOutlet UILabel *lblImsaakTime;
@property (strong, nonatomic) IBOutlet UILabel *lblFajrTime;
@property (strong, nonatomic) IBOutlet UILabel *lblSunriseTime;
@property (strong, nonatomic) IBOutlet UILabel *lblZoharTime;
@property (strong, nonatomic) IBOutlet UILabel *lblSunsetTime;
@property (strong, nonatomic) IBOutlet UILabel *lblMaghribTime;
@property (strong, nonatomic) IBOutlet UILabel *lblMidnightTime;

@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch1;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch2;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch3;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch4;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch5;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch6;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Switch7;


@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;
@property (strong, nonatomic) IBOutlet UIImageView *img4;
@property (strong, nonatomic) IBOutlet UIImageView *img5;
@property (strong, nonatomic) IBOutlet UIImageView *img6;
@property (strong, nonatomic) IBOutlet UIImageView *img7;


@end
