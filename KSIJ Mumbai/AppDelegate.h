//
//  AppDelegate.h
//  KSIJ Mumbai
//
//  Created by Kishan on 20/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "HTTPClient.h"


#include <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UIAlertViewDelegate,HTTPClientDeleagte>
{
    AVAudioPlayer *audioPlayer;
    UIAlertView *objalert;
    NSMutableDictionary *dict;
    HTTPClient *ClientNotiList;
    HTTPClient *ClientGetList,*ClientNoti;


}
@property (strong, nonatomic) UIWindow *window;
@property (strong , nonatomic) UINavigationController *Navig;

@end

