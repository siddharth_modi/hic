//
//  NotificationSubCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 14/12/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSubCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblDateTime;
@property (strong, nonatomic) IBOutlet UILabel *lblEventDetail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_eventDetailsss;

@end
