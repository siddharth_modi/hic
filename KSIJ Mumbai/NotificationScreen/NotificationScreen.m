//
//  NotificationScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "NotificationScreen.h"

@interface NotificationScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    HTTPClient *ClientNotiList;
    NSMutableArray *ArrNotiList;
    
    
    
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
}
@end

@implementation NotificationScreen
@synthesize viewNoti;
@synthesize TblNotiList;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    viewNoti.layer.borderWidth = 1;
    viewNoti.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [viewNoti setHidden:YES];
    
    _view_noNotification.layer.borderWidth = 1;
    _view_noNotification.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [_view_noNotification setHidden:YES];

    
    self.TblNotiList.tableFooterView = [[UIView alloc] init];
    self.TblNotiList.backgroundColor=[UIColor clearColor];
   
}
-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
#pragma mark - RKTabViewDelegate
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
        
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
       
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [self GetNotificationList];
}



#pragma mark - GetNotiFicationList
-(void)GetNotificationList
{
    [SVProgressHUD show];
    
    NSString *registerURL = [NSString stringWithFormat:@"%@get_alert_data_web.php",kAPIURL];
    ClientNotiList = [[HTTPClient alloc] init];
    ClientNotiList.delegate = self;
    [ClientNotiList getResponseFromAPI:registerURL andParameters:nil];
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response home screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client == ClientNotiList)
    {
        NSString *AltListcount=[[response objectForKey:@"alert_list_count"] stringValue];
        
        if (AltListcount!=0)
        {
            ArrNotiList=[[NSMutableArray alloc]init];
            ArrNotiList=[response objectForKey:@"alert_list"];
            
            if (ArrNotiList.count!=0)
            {
                _view_noNotification.hidden=YES;
            }
            else
            {
                _view_noNotification.hidden=NO;
            }
            [TblNotiList reloadData];
        }
        else
        {
            [viewNoti setHidden:NO];
        }
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrNotiList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    NotificationCell *cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    cell.lblDateTime.text =[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"alert_date_format"];
    cell.lblEventDetail.text=[[ArrNotiList objectAtIndex:indexPath.row] objectForKey:@"message_title"];
    cell.lblEventDetail.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    
    [cell.lblEventDetail sizeToFit];
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationSubScreen *vc=[[NotificationSubScreen alloc]initWithNibName:@"NotificationSubScreen" bundle:nil];
    
    NSMutableArray *Temp=[[NSMutableArray alloc]init];
    [Temp addObject:[ArrNotiList objectAtIndex:indexPath.row]];
    vc.ArrpassDetail=Temp;
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark -
#pragma mark - button Notification
- (IBAction)btnNoti:(id)sender
{
    [self GetNotificationList];
}

#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
