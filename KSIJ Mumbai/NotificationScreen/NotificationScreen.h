//
//  NotificationScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "NotificationCell.h"
#import "NotificationSubScreen.h"

@interface NotificationScreen : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIView *viewNoti;
@property (strong, nonatomic) IBOutlet UITableView *TblNotiList;
@property (weak, nonatomic) IBOutlet UIView *view_noNotification;

- (IBAction)btnNoti:(id)sender;

@end
