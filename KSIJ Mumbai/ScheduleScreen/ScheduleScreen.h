//
//  ScheduleScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "DonationScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"
#import "ScheduleSubScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "ScheduleCell.h"

@interface ScheduleScreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
}
@property (strong, nonatomic) IBOutlet UITableView *TblSchedules;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic)NSMutableArray *ArrEvt;
- (IBAction)btnNoti:(id)sender;

@end
