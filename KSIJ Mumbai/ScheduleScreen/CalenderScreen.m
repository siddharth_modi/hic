//
//  CalenderScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 10/11/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "CalenderScreen.h"


@interface CalenderScreen ()<RKTabViewDelegate>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
    NSMutableDictionary *_eventsByDate;
    NSDate *_dateSelected;
    
    NSMutableArray *ArrEventFinal;
    // BOOL isTokonValid;
    
    NSMutableArray *ArrColordate;
    
}
@end

@implementation CalenderScreen

@synthesize ArrEvent,lblTitle;
@synthesize viewHeader,viewHeaderMain;
@synthesize viewFooter;
@synthesize viewNodata;
@synthesize PasSelectedDate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconY"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconW"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    self.tblCalenderEvent.tableFooterView = [[UIView alloc] init];
    self.tblCalenderEvent.backgroundColor=[UIColor clearColor];
    
    self.tblCalenderEvent.rowHeight =UITableViewAutomaticDimension;
    self.tblCalenderEvent.estimatedRowHeight = 150;
    
    
    
    ArrEventFinal=[[NSMutableArray alloc]init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *CurentDate=[dateFormatter stringFromDate:[NSDate date]];
    
    for (int i=0; i<ArrEvent.count; i++)
    {
        
        NSString *apidate=[[ArrEvent objectAtIndex:i] objectForKey:@"EventDate"];
        
        if ([apidate isEqualToString:CurentDate])
        {
            [ArrEventFinal addObject:[ArrEvent objectAtIndex:i]];
        }
    }
    
    if (ArrEventFinal.count!=0)
    {
        lblTitle.text=[[[ArrEventFinal objectAtIndex:0] objectForKey:@"EventTitle"] uppercaseString];
        [viewNodata setHidden:YES];
    }
    else
    {
        [viewNodata setHidden:NO];
    }
    
    
    lblTitle.backgroundColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    
    
    viewHeaderMain.layer.borderWidth = 1;
    viewHeaderMain.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    viewFooter.layer.borderWidth = 1;
    viewFooter.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.tblCalenderEvent.tableHeaderView = viewHeader;
    
    if (ArrEventFinal.count==0)
    {
         self.tblCalenderEvent.tableFooterView = viewFooter;
    }
    
    
    
    
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // set selected Event
    _dateSelected=[self TimeFormateChange3:PasSelectedDate];

    //create Event
    [self createRandomEvents];
    
    _calendarMenuView.contentRatio = .75;
    _calendarManager.settings.weekDayFormat = JTCalendarWeekDayFormatShort;
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:viewHeaderMain];
    [_calendarManager setDate:[NSDate date]];
    
}

-(NSDate *)TimeFormateChange3:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss ZZZ";
    return date;
}

-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
    
}
#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

#pragma mark- Curent date and time
-(NSString *)getTimeStamp
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    NSTimeZone *gmt = [NSTimeZone systemTimeZone];
    [dateFormatter setTimeZone:gmt];
    NSString *timeStamp = [dateFormatter stringFromDate:[NSDate date]];
   
    return timeStamp;
}
- (void)btnAddCalender:(UIButton *)sender
{
    NSString *Timestamp=[NSString stringWithFormat:@"%@",[self getTimeStamp]];
    
    NSString *datestr=[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventDate"];
    NSString *timestr=[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventStartChange"];
   
    NSString *finalstr=[NSString stringWithFormat:@"%@ %@",datestr,timestr];
    
    
    NSString *apidate=[self TimeFormateChange1:finalstr];
    
    
    
    NSDateFormatter *datePickerFormat = [[NSDateFormatter alloc] init];
    [datePickerFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *currentDate = [datePickerFormat dateFromString:Timestamp];
    NSDate *serverDate = [datePickerFormat dateFromString:apidate];
    
    NSLog(@"date %@",currentDate);
    NSLog(@"date %@",serverDate);
    
    NSComparisonResult result;
    result = [currentDate compare:serverDate]; // comparing two dates
    
    if(result == NSOrderedAscending)
    {
        NSLog(@"current date is less");
        
        NSString *sTitle=[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventTitle"];
        NSString *sTime=[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventStartChange"];
        NSString *sLocation=[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventLocation"];
        NSString *FString=[NSString stringWithFormat:@"%@ :-   %@, %@",sTitle,sTime,sLocation];
        
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = serverDate;
        //localNotification.fireDate=[NSDate dateWithTimeIntervalSinceNow:30];
        localNotification.alertBody = FString;
        localNotification.soundName = @"alarm_sound.m4r";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        NSMutableDictionary * userinfo=[[NSMutableDictionary alloc]init];
        [userinfo setValue:[[ArrEventFinal objectAtIndex:sender.tag] objectForKey:@"EventTitle"] forKey:@"AlarmName"];
        [userinfo setValue:@"10" forKey:@"NT"];
        localNotification.userInfo=userinfo;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
    }
    else if(result == NSOrderedDescending)
    {
        NSLog(@"server date is less");
    }
    else if(result == NSOrderedSame)
    {
        NSLog(@"Both dates are same");
    }
    else
    {
        NSLog(@"Date cannot be compared");
    }
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:kAlertViewTitle message:@"Event has been added to calander" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}
//#pragma mark - Cumpare Two date
//-(BOOL)dateComparision:(NSDate*)date1 andDate2:(NSDate*)date2
//{
//    BOOL isTokonValid;
//    if ([date1 compare:date2] == NSOrderedDescending)
//    {
//        //"date1 is later than date2
//        isTokonValid = YES;
//    }
//    else if ([date1 compare:date2] == NSOrderedAscending)
//    {
//        //date1 is earlier than date2
//        isTokonValid = NO;
//    }
//    else
//    {
//        //dates are the same
//        isTokonValid = NO;
//    }
//
//    return isTokonValid;
//}
-(NSString *)TimeFormateChange1:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd hh:mm a";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
    return [dateFormatter stringFromDate:date];
}
#pragma mark -
#pragma mark - Tableview Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return ArrEventFinal.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    
    EventCell *cell = (EventCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    NSString *str1=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"EventDateChange"];
    NSString *str2=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"EventStartChange"];
    
    cell.lbldateTime.text =[NSString stringWithFormat:@"%@ %@",str1,str2];
    NSString *str_EventTitle=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"EventTitle"];
    NSString *str2_EventDetail=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"EventDetails"];
    
    if ([str2_EventDetail  isEqualToString:@""])
    {
        cell.lblEventTitle.text=str_EventTitle;
    }
    else
    {
        cell.lblEventTitle.text=[NSString stringWithFormat:@"%@ \n%@",str_EventTitle,str2_EventDetail];
        
    }
    NSString *str_speaker=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"Speaker"];
    NSString *str_speakerDetail=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"SpeakerDetails"];
    
    if ([str_speakerDetail  isEqualToString:@""])
    {
        cell.lbl_speaker.text=str_speaker;
    }
    else
    {
        cell.lbl_speaker.text=[NSString stringWithFormat:@"%@ \n%@",str_speaker,str_speakerDetail];
        
    }
    
    cell.lblLocationTitle.text=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"EventLocation"];
    cell.btnAddCalender.tag=indexPath.row;
    [cell.btnAddCalender addTarget:self action:@selector(btnAddCalender:)forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *color=[[ArrEventFinal objectAtIndex:indexPath.row] objectForKey:@"ColorCode"];

    if ([color isEqualToString:@"B"])
    {
        lblTitle.backgroundColor=[UIColor blackColor];
        cell.lblEventTitle.textColor=[UIColor blackColor];
    }
    else if ([color isEqualToString:@"R"])
    {
        lblTitle.backgroundColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        cell.lblEventTitle.textColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
    }
    else if ([color isEqualToString:@"G"])
    {
        lblTitle.backgroundColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
        cell.lblEventTitle.textColor=[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
    }

    return cell;
    
}
#pragma mark - DidReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}
#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    
    // Other month
    if([dayView isFromAnotherMonth])
    {
        dayView.hidden = YES;
    }
    // Today
    else if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date])
    {
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor colorWithRed:0.59 green:0.71 blue:0.91 alpha:1];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date])
    {
        NSLog(@"dayviewDate:- %@",_dateSelected);
        
        dayView.circleView.hidden = NO;
        //dayView.circleView.backgroundColor = [UIColor blueColor];
        dayView.circleView.backgroundColor =[UIColor colorWithRed:0.59 green:0.71 blue:0.91 alpha:1];
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        
        //
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSString *SelectedDate = [formatter stringFromDate:_dateSelected];
        
        ArrEventFinal=[[NSMutableArray alloc]init];
        for (int i=0; i<ArrEvent.count; i++)
        {
            NSString *apidate=[[ArrEvent objectAtIndex:i] objectForKey:@"EventDate"];
            if ([apidate isEqualToString:SelectedDate])
            {
                [ArrEventFinal addObject:[ArrEvent objectAtIndex:i]];
            }
        }
        
        if (ArrEventFinal.count!=0)
        {
            lblTitle.text=[[[ArrEventFinal objectAtIndex:0] objectForKey:@"EventTitle"] uppercaseString];
            
            NSString *bgcolorstr=[[ArrEventFinal objectAtIndex:0] objectForKey:@"ColorCode"];
            
            if ([bgcolorstr isEqualToString:@"B"])
            {
                lblTitle.backgroundColor=[UIColor blackColor];
            }
            else if ([bgcolorstr isEqualToString:@"R"])
            {
                lblTitle.backgroundColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
            }
            else if ([bgcolorstr isEqualToString:@"G"])
            {
                lblTitle.backgroundColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
            }
            [viewNodata setHidden:YES];
        }
        else
        {
            lblTitle.text=[[NSString stringWithFormat:@"No Event"] uppercaseString];
            lblTitle.backgroundColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];

            [viewNodata setHidden:NO];
        }
        
        
        [_tblCalenderEvent reloadData];
        self.tblCalenderEvent.tableFooterView = viewFooter;
        [viewFooter setHidden:NO];
        if (ArrEventFinal.count!=0)
        {
            [viewFooter setHidden:YES];
        }
        //
        
    }
    // Another day of the current month
    else
    {
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    
    if([self haveEventForDay:dayView.date])
    {
        //dayView.dotView.hidden = NO;
        
        NSString *st=[NSString stringWithFormat:@"%@",dayView.date];
        NSString *firstWord = [[st componentsSeparatedByString:@" "] objectAtIndex:0];
        NSString *dateviewdate=[self GetNextDate:firstWord];
        
        for (int i=0; i<ArrColordate.count; i++)
        {
            NSString *colordate=[[ArrColordate objectAtIndex:i] objectForKey:@"date"];
            
            if ([dateviewdate isEqualToString:colordate])
            {
                NSString *colorcode=[[ArrColordate objectAtIndex:i] objectForKey:@"color"];
                
                if ([colorcode isEqualToString:@"R"])
                {
                    dayView.circleView.backgroundColor =[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
                }
                else if ([colorcode isEqualToString:@"G"])
                {
                    dayView.circleView.backgroundColor =[UIColor colorWithRed:0.19 green:0.40 blue:0.04 alpha:1];
                }
                else if ([colorcode isEqualToString:@"B"])
                {
                    dayView.circleView.backgroundColor =[UIColor blackColor];
                }
            }
        }
        
        dayView.circleView.hidden = NO;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        
        
    }
    else
    {
        //dayView.dotView.hidden = YES;
    }
}

-(NSString*)GetNextDate:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *dateIncremented= [calendar dateByAddingComponents:components toDate:dateFromString options:0];
    
    NSDateFormatter *myDateFormatter = [[NSDateFormatter alloc] init];
    [myDateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *stringFromDate = [myDateFormatter stringFromDate:dateIncremented];
    
    return stringFromDate;
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
//    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
//    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//    [dateformate setTimeZone:timeZone];
//    [dateformate setDateFormat:@"yyyy-MM-dd HH	:mm:ss"];
//    NSString *date = [dateformate stringFromDate:_dateSelected];
//    NSLog(@"Selected Date :%@",date);
    
    
   

    
    
   // NSLog(@"%@",_dateSelected);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *SelectedDate = [formatter stringFromDate:_dateSelected];
    
    ArrEventFinal=[[NSMutableArray alloc]init];
    for (int i=0; i<ArrEvent.count; i++)
    {
        NSString *apidate=[[ArrEvent objectAtIndex:i] objectForKey:@"EventDate"];
        if ([apidate isEqualToString:SelectedDate])
        {
            [ArrEventFinal addObject:[ArrEvent objectAtIndex:i]];
        }
    }
    
    if (ArrEventFinal.count!=0)
    {
        lblTitle.text=[[[ArrEventFinal objectAtIndex:0] objectForKey:@"EventTitle"] uppercaseString];
        NSString *bgcolorstr=[[ArrEventFinal objectAtIndex:0] objectForKey:@"ColorCode"];
        
        if ([bgcolorstr isEqualToString:@"B"])
        {
            lblTitle.backgroundColor=[UIColor blackColor];
        }
        else if ([bgcolorstr isEqualToString:@"R"])
        {
            lblTitle.backgroundColor=[UIColor colorWithRed:0.93 green:0.17 blue:0.05 alpha:1];
        }
        else if ([bgcolorstr isEqualToString:@"G"])
        {
            lblTitle.backgroundColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
        }
        
        [viewNodata setHidden:YES];
    }
    else
    {
        lblTitle.text=[[NSString stringWithFormat:@"No Event"] uppercaseString];
        lblTitle.backgroundColor=[UIColor colorWithRed:0.47 green:0.63 blue:0.42 alpha:1];
        [viewNodata setHidden:NO];
    }
    
    [_tblCalenderEvent reloadData];
    self.tblCalenderEvent.tableFooterView = viewFooter;
    [viewFooter setHidden:NO];
    if (ArrEventFinal.count!=0)
    {
        [viewFooter setHidden:YES];
    }
    
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled)
    {
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:viewHeaderMain.date isTheSameMonthThan:dayView.date])
    {
        if([viewHeaderMain.date compare:dayView.date] == NSOrderedAscending)
        {
            [viewHeaderMain loadNextPageWithAnimation];
        }
        else
        {
            [viewHeaderMain loadPreviousPageWithAnimation];
        }
    }
}



#pragma mark - Views customization

- (UIView *)calendarBuildMenuItemView:(JTCalendarManager *)calendar
{
    UILabel *label = [UILabel new];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"Avenir-Medium" size:16];
    
    return label;
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [dateFormatter stringFromDate:date];
}

- (UIView<JTCalendarWeekDay> *)calendarBuildWeekDayView:(JTCalendarManager *)calendar
{
    JTCalendarWeekDayView *view = [JTCalendarWeekDayView new];
    
    for(UILabel *label in view.dayViews){
        label.textColor = [UIColor blackColor];
        label.font = [UIFont boldSystemFontOfSize:14];
        label.backgroundColor=[UIColor colorWithRed:0.20 green:0.43 blue:0.83 alpha:1];
        label.textColor=[UIColor whiteColor];
        
    }
    
    return view;
}

- (UIView<JTCalendarDay> *)calendarBuildDayView:(JTCalendarManager *)calendar
{
    JTCalendarDayView *view = [JTCalendarDayView new];
    
    view.textLabel.font = [UIFont boldSystemFontOfSize:14];
    view.layer.borderWidth=0.5;
    view.layer.borderColor=[UIColor grayColor].CGColor;
//    view.circleRatio = .8;
//    view.dotRatio = 1. / .9;
    
    return view;
}

#pragma mark - Fake data

// Used only to have a key for _eventsByDate
- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter)
    {
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
   _eventsByDate = [NSMutableDictionary new];
    
    for (int i=0; i<ArrEvent.count; i++)
    {
        NSString *date = [[ArrEvent objectAtIndex:i] valueForKey:@"EventDate"];
        NSString *key = [self TimeFormateChange2:date];
        if(!_eventsByDate[key])
        {
            _eventsByDate[key] = [NSMutableArray new];
        }
        [_eventsByDate[key] addObject:date];
    }
    
    
    
    
    //create date and color code Array
    ArrColordate=[[NSMutableArray alloc]init];
    for (int j=0; j<ArrEvent.count; j++)
    {
        NSString *dateis=[[ArrEvent objectAtIndex:j] objectForKey:@"EventDate"];
        NSString *coloris=[[ArrEvent objectAtIndex:j] objectForKey:@"ColorCode"];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:dateis forKey:@"date"];
        [dict setValue:coloris forKey:@"color"];
        [ArrColordate addObject:dict];
    }
   // NSLog(@"colorArr %@",ArrColordate);
    
}

-(NSString *)TimeFormateChange2:(NSString *)DateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd";
    NSDate *date = [dateFormatter dateFromString:DateStr];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    return [dateFormatter stringFromDate:date];
}


@end
