//
//  EventCell.m
//  KSIJ Mumbai
//
//  Created by Kishan on 10/11/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell
@synthesize btnAddCalender;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    btnAddCalender.clipsToBounds = YES;
    btnAddCalender.layer.cornerRadius =5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
