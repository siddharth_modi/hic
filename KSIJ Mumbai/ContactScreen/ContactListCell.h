//
//  ContactListCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 25/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbladdress;


@property (strong, nonatomic) IBOutlet UIView *p1;
@property (strong, nonatomic) IBOutlet UIView *p2;
@property (strong, nonatomic) IBOutlet UIView *p3;

@property (strong, nonatomic) IBOutlet UILabel *lblPhone1Title;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone2title;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone3Title;

@property (strong, nonatomic) IBOutlet UIButton *btnphone1;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone2;
@property (strong, nonatomic) IBOutlet UIButton *btnFax;
@property (strong, nonatomic) IBOutlet UIButton *btnMail;
@property (strong, nonatomic) IBOutlet UIButton *btnWeb;

@property (strong, nonatomic) IBOutlet UIImageView *imgPhone;
@property (strong, nonatomic) IBOutlet UIImageView *imgMail;
@property (strong, nonatomic) IBOutlet UIImageView *imgWeb;

@end
