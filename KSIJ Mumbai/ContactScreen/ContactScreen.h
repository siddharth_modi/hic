//
//  ContactScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 21/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>

#import "ContactListCell.h"
#import <MessageUI/MessageUI.h>

@interface ContactScreen : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}
@property (nonatomic, strong) UITextField *activeTextField;

@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UIView *viewSub;
@property (strong, nonatomic) IBOutlet UITableView *tblContactList;
@property (strong, nonatomic) IBOutlet UIView *ViewFutter;

@property (strong, nonatomic) IBOutlet UIView *viewSocial;
@property (strong, nonatomic) IBOutlet UIView *viewSubscribe;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;

@property (strong, nonatomic) IBOutlet UITextField *txtMail;
@property (strong, nonatomic) IBOutlet UITextField *txtMobileNo;

- (IBAction)btnNoti:(id)sender;
- (IBAction)btnSend:(id)sender;
- (IBAction)btnFb:(id)sender;
- (IBAction)btnTwetter:(id)sender;
- (IBAction)btnyoutube:(id)sender;

@end
