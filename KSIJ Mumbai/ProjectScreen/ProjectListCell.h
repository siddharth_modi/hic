//
//  ProjectListCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 24/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectListCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblname;

@end
