//
//  MediaCell.m
//  KSIJ Mumbai
//
//  Created by Kishan on 26/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "MediaCell.h"

@implementation MediaCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
