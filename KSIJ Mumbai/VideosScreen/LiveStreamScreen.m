//
//  LiveStreamScreen.m
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import "LiveStreamScreen.h"

@interface LiveStreamScreen ()<RKTabViewDelegate,HTTPClientDeleagte>
{
    RKTabItem *FirstTabItem,*SecondTabItem,*ThirdTabItem,*FourTabItem,*FiveTabItem,*SixTabItem;
    
    HTTPClient *ClientContactList;
    
    NSString *LiveStreamID;
    NSMutableArray *ArrVideoList;
    NSString *pageToken;
    NSString *PreviosToken;
    
    NSMutableArray *arr_LiveStreamVideo;
    
}
@end

@implementation LiveStreamScreen
@synthesize imgVideo;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arr_LiveStreamVideo=[[NSMutableArray alloc] init];
    
    FirstTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprayIconY"] imageDisabled:[UIImage imageNamed:@"TabprayIconW"]];
    
    SecondTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TdonateIconY"] imageDisabled:[UIImage imageNamed:@"TdonateIconW"]];
    
    ThirdTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcalendarIconY"] imageDisabled:[UIImage imageNamed:@"TabcalendarIconW"]];
    
    FourTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabprojectIconY"] imageDisabled:[UIImage imageNamed:@"TabprojectIconW"]];
    
    FiveTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabvideoIconY"] imageDisabled:[UIImage imageNamed:@"TabvideoIconY"]];
    
    SixTabItem = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"TabcontactBookIconY"] imageDisabled:[UIImage imageNamed:@"TabcontactBookIconW"]];
    
    _btnNext.clipsToBounds = YES;
    _btnNext.layer.cornerRadius =5;
    
    _btnPrevious.clipsToBounds = YES;
    _btnPrevious.layer.cornerRadius =5;
    
    _view_NoVideo.hidden=YES;
    _view_NoVideo.layer.borderWidth = 1;
    _view_NoVideo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    RKTabView *tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-60,self.view.bounds.size.width, 60)];
    tabView.delegate = self;
    tabView.horizontalInsets = HorizontalEdgeInsetsMake(0, 0);;
    
    tabView.backgroundColor=[UIColor colorWithRed:0.43 green:0.63 blue:0.44 alpha:1];
    
    tabView.tabItems =@[FirstTabItem, SecondTabItem, ThirdTabItem, FourTabItem, FiveTabItem,  SixTabItem];
    
    [self.view addSubview:tabView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self GetChnnelList:nil];
}

#pragma mark -
#pragma mark - TabBar Delegate methods
- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu enabled", index);
    
    if (index==0)
    {
        HomeScreen *vc=[[HomeScreen alloc]initWithNibName:@"HomeScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==1)
    {
        DonationScreen *vc=[[DonationScreen alloc]initWithNibName:@"DonationScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==2)
    {
        ScheduleSubScreen *vc=[[ScheduleSubScreen alloc]initWithNibName:@"ScheduleSubScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];

    }
    else if (index==3)
    {
        ProjectScreen *vc=[[ProjectScreen alloc]initWithNibName:@"ProjectScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==4)
    {
        VideosScreen *vc=[[VideosScreen alloc]initWithNibName:@"VideosScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    else if (index==5)
    {
        ContactScreen *vc=[[ContactScreen alloc]initWithNibName:@"ContactScreen" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
}
- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(NSUInteger)index tab:(RKTabItem *)tabItem
{
    NSLog(@"Tab %tu disabled", index);
}

#pragma mark - Button Notification
- (IBAction)btnNoti:(id)sender
{
    NotificationScreen *vc=[[NotificationScreen alloc]initWithNibName:@"NotificationScreen" bundle:nil];
    [self.navigationController pushViewController:vc animated:NO];
}

- (IBAction)btnPlayVideo:(id)sender
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",LiveStreamID]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",LiveStreamID]]];
    }
}

#pragma mark - didReceiveMemoryWarning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - GetContactDeatil
-(void)GetContactDeatil
{
    [SVProgressHUD show];
    NSString *registerURL = [NSString stringWithFormat:@"%@setting_web.php",kAPIURL];
    ClientContactList = [[HTTPClient alloc] init];
    ClientContactList.delegate = self;
    [ClientContactList getResponseFromAPI:registerURL andParameters:nil];
}
-(void)GetImagefromUrl
{
    NSString *str1=@"http://img.youtube.com/vi/";
    NSString *str2=@"/0.jpg";
    NSString *url=[NSString stringWithFormat:@"%@%@%@",str1,LiveStreamID,str2];
    
    [imgVideo setShowActivityIndicatorView:YES];
    [imgVideo setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [imgVideo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"no-image"]];
    imgVideo.contentMode=UIViewContentModeScaleToFill;
}

#pragma mark -
#pragma mark - HTTPClient Delegate methods
- (void)httpClient:(HTTPClient *)client didUpdateWithResponse:(id)response
{
    //NSLog(@"Response Project screen: %@",response);
    
    [SVProgressHUD dismiss];
    if (client== ClientContactList)
    {
        LiveStreamID=[[response objectForKey:@"setting"] objectForKey:@"youtube_id_livestrem"];
        
        [self GetImagefromUrl];
        
    }
}
- (void)httpClient:(HTTPClient *)client didFailedWithError:(NSError *)error
{
    [SVProgressHUD dismiss];
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark - GetChnnelList
-(void)GetChnnelList:(NSString *)Token
{
    [SVProgressHUD show];
    
    
    NSString *urlString;
    if ([Token length]>0)
    {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDvrxZvufNw41vMEj41j-qMbN5S1Rt4w0E&channelId=UC0TyG6D_FEcBmAfCCyilgZw&part=snippet,id&order=date&maxResults=50&pageToken=%@",Token];
    }
    else
    {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?key=AIzaSyDvrxZvufNw41vMEj41j-qMbN5S1Rt4w0E&channelId=UC0TyG6D_FEcBmAfCCyilgZw&part=snippet,id&order=date&maxResults=50"];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
     {
         
         
         ArrVideoList=[[NSMutableArray alloc] init];
         
         ArrVideoList=[responseObject objectForKey:@"items"];
         
          NSLog(@"JSON: %@", ArrVideoList);
         
         
         pageToken = [responseObject valueForKey:@"nextPageToken"];
         
          NSLog(@" %@", pageToken);
         
         PreviosToken=[responseObject valueForKey:@"prevPageToken"];
         
            if (pageToken!=nil)
            {
                if (ArrVideoList.count==0)
                {
                     [self GetChnnelList:pageToken];
                }
                else
                {
                    for (int i=0; i<ArrVideoList.count; i++)
                    {
                        NSLog(@"%@",[ArrVideoList objectAtIndex:i]);
                        
                        NSString *str_liveBroadcast=[NSString stringWithFormat:@"%@",[[[ArrVideoList objectAtIndex:i] objectForKey:@"snippet"] objectForKey:@"liveBroadcastContent"]];
                        if ([str_liveBroadcast isEqualToString:@"live"])
                        {
                            [arr_LiveStreamVideo addObject:[ArrVideoList objectAtIndex:i]];
                        }
                        if (i==ArrVideoList.count-1)
                        {
                            [self GetChnnelList:pageToken];
                        }
                        NSLog(@"%@",str_liveBroadcast);
                        
                    }

                }
           }
         else
         {
             [SVProgressHUD dismiss];
             
             
             ArrVideoList=arr_LiveStreamVideo.mutableCopy;
             NSLog(@"%@",ArrVideoList);
             if (arr_LiveStreamVideo.count==0 || arr_LiveStreamVideo==nil)
             {
                 _view_NoVideo.hidden=NO;
             }
             else
             {
                  _view_NoVideo.hidden=YES;
             }
             [_tblChanleList reloadData];
             
             // self.tblChanleList.tableHeaderView = viewHeader;
            // self.tblChanleList.tableFooterView = _viewFooter;
             
             if (PreviosToken==nil)
             {
                 [_btnPrevious setHidden:YES];
             }
             else
             {
                 [_btnPrevious setHidden:NO];
             }
             
             if (pageToken==nil)
             {
                 [_btnNext setHidden:YES];
             }
             else
             {
                 [_btnNext setHidden:NO];
             }
             
             [_tblChanleList setContentOffset:CGPointZero animated:YES];

     }
//
//         ArrVideoList=[[NSMutableArray alloc]init];
//         ArrVideoList=[responseObject objectForKey:@"items"];
         
     }
         failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         
         [SVProgressHUD dismiss];
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:kAlertViewTitle message:error.localizedDescription delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
}

#pragma mark -
#pragma mark - Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ArrVideoList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *headerView = [[UIView alloc] init];
//    headerView.backgroundColor = [UIColor clearColor];
//    return headerView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"StatusCellIdentifier";
    MediaCell *cell = (MediaCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MediaCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    cell.lbltitle.text = [[[ArrVideoList objectAtIndex:indexPath.section] objectForKey:@"snippet"] objectForKey:@"title"];
    
    NSString *url1=[[[[[ArrVideoList objectAtIndex:indexPath.section]objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"medium"] objectForKey:@"url"];
    [cell.imgVideo setShowActivityIndicatorView:YES];
    [cell.imgVideo setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [cell.imgVideo sd_setImageWithURL:[NSURL URLWithString:url1] placeholderImage:[UIImage imageNamed:@"no-image"]];
    cell.imgVideo.contentMode=UIViewContentModeScaleToFill;
    
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *vId = [[[ArrVideoList objectAtIndex:indexPath.section] valueForKey:@"id"] valueForKey:@"videoId"];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",vId]]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",vId]]];
    }
}

- (IBAction)btnNext:(id)sender
{
    [self GetChnnelList:pageToken];
}

- (IBAction)btnprevios:(id)sender
{
    [self GetChnnelList:PreviosToken];
}


@end
