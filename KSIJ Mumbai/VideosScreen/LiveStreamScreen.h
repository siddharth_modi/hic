//
//  LiveStreamScreen.h
//  KSIJ Mumbai
//
//  Created by Kishan on 27/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RKTabView.h>
#import "HomeScreen.h"
#import "DonationScreen.h"
#import "ScheduleScreen.h"
#import "ProjectScreen.h"
#import "VideosScreen.h"
#import "ContactScreen.h"
#import "NotificationScreen.h"

#import "Constants.h"
#import "AFNetworking.h"
#import "HTTPClient.h"
#import "SVProgressHUD.h"
#import <UIImageView+WebCache.h>


@interface LiveStreamScreen : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *imgVideo;
@property (strong, nonatomic) IBOutlet UITableView *tblChanleList;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnPrevious;
- (IBAction)btnNoti:(id)sender;
- (IBAction)btnPlayVideo:(id)sender;
- (IBAction)btnNext:(id)sender;
- (IBAction)btnprevios:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *view_NoVideo;
@end
