//
//  MediaCell.h
//  KSIJ Mumbai
//
//  Created by Kishan on 26/10/16.
//  Copyright © 2016 Latitudetechnolabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lbltitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgVideo;

@end
